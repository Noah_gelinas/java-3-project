/*Package for reading from tables*/
CREATE OR REPLACE PACKAGE java_reads AS
    TYPE table_cursor IS REF CURSOR;
    FUNCTION read_customers RETURN table_cursor;
    FUNCTION read_merch RETURN table_cursor;
    FUNCTION read_games RETURN table_cursor;
END java_reads;
/
CREATE OR REPLACE PACKAGE BODY java_reads AS
    e_invalid_input EXCEPTION;
    
    FUNCTION read_customers RETURN table_cursor 
        IS
            customer_table table_cursor;
        BEGIN
            OPEN customer_table FOR 
            SELECT c.*
            FROM Java_Customers c;
            RETURN customer_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
                
    FUNCTION read_merch RETURN table_cursor 
        IS
            merch_table table_cursor;
        BEGIN
            OPEN merch_table FOR 
            SELECT *
            FROM Java_Merchandise;
            RETURN merch_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    FUNCTION read_games RETURN table_cursor 
        IS
            games_table table_cursor;
        BEGIN
            OPEN games_table FOR 
            SELECT *
            FROM Java_Games;
            RETURN games_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
END java_reads;
    