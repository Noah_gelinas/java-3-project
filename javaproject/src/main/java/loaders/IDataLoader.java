package loaders;

import java.util.*;
import product.IProduct;
import java.io.*; 
import customers.Customer;

/**
 * IDataLoader is an interface which defines the following methods and is implemented by DatabaseLoader and FileLoader
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public interface IDataLoader {

    /**
     * reads the products from the user input file
     * @param filename filename input by user
     * @return List of products
     * @throws IOException 
     * @throws FileNotFoundException
     * @throws NumberFormatException
     */
    List<IProduct> loadProducts(String filename) throws IOException, FileNotFoundException, NumberFormatException;

    /**
     * adds new products to the file
     * @param filename filename entered by user to add new products to
     * @param products List of products 
     * @throws IOException
     */
    void sendProducts(String filename, List<IProduct> products) throws IOException;

    /**
     * reads the customers from the user input file
     * @param filename filename input by user
     * @return List of customers
     * @throws IOException
     * @throws FileNotFoundException
     * @throws NumberFormatException
     */
    List<Customer> loadCustomers(String filename) throws IOException, FileNotFoundException, NumberFormatException;

    /**
     * adds new customers to the file
     * @param filename filename entered by user to add new customers to
     * @param customers List of customers 
     * @throws IOException
     */
    void sendCustomers(String filename, List<Customer>customers) throws IOException;
}
