package loaders;

import java.nio.file.*;
import java.util.*;
import product.*;
import java.io.*;
import customers.Customer; 

/**
 * FileLoader is used to read and store data found on a file
 * @author Noah Gelinas and Jaypee Tello
 * @version 11-25-2023
 */
public class FileLoader implements IDataLoader{

    /**
     * reads the products from the user input file
     * @param filename filename input by user
     * @return List of products
     * @throws IOException 
     * @throws FileNotFoundException
     * @throws NumberFormatException
     */
    public List<IProduct> loadProducts(String filename) throws IOException, FileNotFoundException, NumberFormatException {
        Path p = Paths.get(filename);
        List<String> lines = Files.readAllLines(p);
        ArrayList<IProduct> products = new ArrayList<IProduct>();
        for (String line : lines) {
            String[] pieces = line.split(",");
            if (pieces.length == 7) {
                products.add(new Merchandise(Integer.parseInt(pieces[0]), (pieces[1].charAt(0)), pieces[2], Double.parseDouble(pieces[3]), pieces[4], Double.parseDouble(pieces[5]), Integer.parseInt(pieces[6])));
            } else if (pieces.length == 8) {
                products.add(new Game(Integer.parseInt(pieces[0]), pieces[1], Double.parseDouble(pieces[2]), pieces[3], Double.parseDouble(pieces[4]), pieces[5], Double.parseDouble(pieces[6]), Integer.parseInt(pieces[7])));
            }
        }     
        return products;
    }

    /**
     * adds new products to the file
     * @param filename filename entered by user to add new products to
     * @param products List of products 
     * @throws IOException
     */
  public void sendProducts(String filename, List<IProduct> products) throws IOException {
    List<String> productsData = new ArrayList<String>();
    for (IProduct product : products) {
      productsData.add(product.getProductData());
    }
    Files.write(Paths.get(filename), productsData);
  }

  /**
   * reads customers from user input file
   * @param filename filename input by user
   * @return List of customers
   * @throws IOException
   * @throws FileNotFoundException
   * @throws NumberFormatException
   */
  public List<Customer> loadCustomers(String filename) throws IOException,FileNotFoundException,NumberFormatException {
    Path p = Paths.get(filename);
        List<String> lines = Files.readAllLines(p);
        ArrayList<Customer> customers = new ArrayList<Customer>();
        for (String line : lines) {
            String[] pieces = line.split(",");
            customers.add(new Customer(Integer.parseInt(pieces[0]),pieces[1],pieces[2],Integer.parseInt(pieces[3])));
        }     
        return customers;
  }

  /**
     * adds new customers to the file
     * @param filename filename entered by user to add new customers to
     * @param customers List of customers 
     * @throws IOException
     */
  public void sendCustomers(String filename, List<Customer>customers) throws IOException {
    List<String> customersData = new ArrayList<String>();
    for (Customer customer : customers) {
      customersData.add(customer.getCustomerData());
    }
    Files.write(Paths.get(filename),customersData);
  }
}