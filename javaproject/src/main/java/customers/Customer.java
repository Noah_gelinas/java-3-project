package customers;

/**
 * Customer is a class which creates the Customer object
 * @author Noah Gelinas and Jaypee Tello
 * @version 11-29-2023
 */
public class Customer {
    private int index;
    private String firstname;
    private String lastname;
    private int points;

    /**
     * Customer constructor to create customer object
     * @param index ID of new customer
     * @param firstname firstname of new customer
     * @param lastname lastname of new customer
     * @param points points of new customer
     */
    public Customer(int index,String firstname, String lastname, int points) {
        this.index=index;
        this.firstname=firstname;
        this.lastname=lastname;
        this.points=points;
    }

    /**
     * returns Specific customers id
     * @return id of customer
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * returns firstname of specific customer
     * @return firstname of customer
     */
    public String getFirstName() {
        return this.firstname;
    }

    /**
     * returns lastname of specific customer
     * @return lastname of customer
     */
    public String getLastName() {
        return this.lastname;
    }

    /**
     * returns points of specific customer
     * @return points of customer
     */
    public int getPoints() {
        return this.points;
    }

    /**
     * set new Index of specific customer
     * @param index new id
     */
    public void setIndex(int index) {
        this.index=index;
    } 

    /**
     * set new firstname of specific customer
     * @param firstname new firstname
     */
    public void setFirstName(String firstname) {
        this.firstname=firstname;
    } 

    /**
     * set new lastname of specific customer
     * @param lastname new lastname
     */
    public void setLastName(String lastname) {
        this.lastname=lastname;
    }

    /**
     * set new points of specific customer
     * @param points new points
     */
    public void setPoints(int points) {
        this.points=points;
    }

    /**
    * returns the data of this customer
    * @return a string of all the data from this customer
    */
    public String getCustomerData() {
        return this.index+","+this.firstname+","+this.lastname+","+this.points;
    }

    /**
    * Overrides the toString to print all the data with identifiers
    * @return a string of all the data with its identifiers of this customer
    */
    @Override
    public String toString() {
        return "\nID: " + this.index + "\nFirst Name: " + this.firstname +"\nLast Name: " + this.lastname + "\nPoints: " + this.points;
    }
}
