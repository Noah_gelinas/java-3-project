package javaproject;

import java.util.*;
import java.io.*;
import java.sql.*;
import oracle.jdbc.OracleTypes;

import product.*;
import comparer.*;
import loaders.*;
import customers.*;

/**
 * GameStore is a class which defines all the actions an employee and admin can do.
 * @author Noah Gelinas and Jaypee Tello
 * @version 11-25-2023
 */
public class GameStore {
  List<IProduct> products;
  List<Customer> customers;
  IDataLoader loader;
  List<IProduct> productsChosen;
  List<Customer> customersChosen;
  Connection conn;

  /**
   * Constructor which initializes a new FileLoader object and a new list of products
   * @throws IOException
   */
  public GameStore() throws IOException {
    this.loader = new FileLoader();
    this.products = new ArrayList<IProduct>();
    this.customers = new ArrayList<Customer>();
  }

  /**
   * Reads products based on order given
   * @param order Array which includes what they would like to order by and if it is ascending or descending
   * @param productsChosen List of products with the products to be read 
   */
  public void orderRead(char[] order, List<IProduct> productsChosen) { 
    IProduct[] productsArr = new IProduct[productsChosen.size()];

    for (int i = 0; i < productsChosen.size(); i++) {
      productsArr[i] = productsChosen.get(i);
    }

    if (order[0] == 'P') {
      selectionSort(productsArr, new PriceComparer(), order[1]);
    }
    else if (order[0] == 'R') {
      selectionSort(productsArr, new RatingComparer(), order[1]);
    }
    else if (order[0] == 'C') {
      selectionSort(productsArr, new DiscountComparer(), order[1]);
    }
    else if (order[0] == 'Q') {
      selectionSort(productsArr, new QuantityComparer(), order[1]);
    }
  }

  public void orderCustomersRead(char order, List<Customer> customersChosen) {
    this.customersChosen = customersChosen;
    Customer[] customerArray = new Customer[this.customersChosen.size()];
    for(int i = 0; i < this.customersChosen.size();i++) {
      customerArray[i]=this.customersChosen.get(i);
    }
    selectionSortPoints(customerArray, new PointComparer(), order);
  }

  /**
   * shows specific product data
   * @param productType which product user is working with. Merchandise or Game
   */
  public void showSpecificProducts(char productType, String[] specs, List<IProduct> productsChosen) {
    List<IProduct> specificProducts = new ArrayList<IProduct>();

    for (int i = 0; i < productsChosen.size(); i++) {
      if (specs[0].equals("Creator")) {
        if (productsChosen.get(i).getCreator().contains(specs[1])) {
          specificProducts.add(productsChosen.get(i));
        }
      }
      else if (specs[0].equals("Size")) {
        if (((Merchandise) productsChosen.get(i)).getSize() == specs[1].charAt(0)) {
          specificProducts.add(productsChosen.get(i));
        }
      }
      else if (specs[0].equals("Name")) {
        if (productsChosen.get(i).getName().contains(specs[1])) {
          specificProducts.add(productsChosen.get(i));
        }
      }
      else if (specs[0].equals("Genre")) {
        if (((Game) productsChosen.get(i)).getGenre().contains(specs[1])) {
          specificProducts.add(productsChosen.get(i));
        }
      }
      else if (specs[0].equals("Index")) {
        try {
          if (productsChosen.get(i).getIndex() == (Integer.parseInt(specs[1]))) {
            specificProducts.add(productsChosen.get(i));
          }
        }
        catch (NumberFormatException nfe) {
          System.out.println("ID invalid!");
          i = productsChosen.size();
        }
        catch (StringIndexOutOfBoundsException sioobe) {
          errorMessage();
        }
      }
    }

    if (specificProducts.size() == 0) {
      System.out.println("\nERROR 404: Product Not Found.\n");
    }
    else {
      showList(specificProducts);
    }
  }

  /**
   * finds a specific customer to display
   * @param customerID id of customer to display
   * @param customersChosen List of customers to look through
   */
  public void showSpecificCustomer(String customerID, List<Customer> customersChosen) {
    List<Customer> specificCustomers = new ArrayList<Customer>();
    this.customersChosen = customersChosen;

    try {
      specificCustomers.add(this.customersChosen.get(Integer.parseInt(customerID)));
    }
    catch (NumberFormatException nfe) {
      System.out.println("ID invalid!");
    }
    catch (IndexOutOfBoundsException sioobe) {
      errorMessage();
    }

    if (specificCustomers.size() == 0) {
      System.out.println("\nERROR 404: Customer Not Found.\n");
    }
    else {
      showListCustomers(specificCustomers);
    }
  }

  /**
   * Sorts the data in the list
   * @param products array of Products
   * @param comp comparer object
   * @param order ascending or descending
   */
  public void selectionSort(IProduct[] products, IProductComparer comp, char order) {
    for (int i = 0; i < products.length - 1; i++) {
      for (int j = i + 1; j < products.length; j++) {
        if(comp.compareProducts(products[j], products[i], order) < 0) {
          products = swap(products, i, j);
        }
      }
    }

    for (IProduct product : products) {
      System.out.println(product);
    }
  }

  /**
   * Sorts the data in the list
   * @param customers List of customers to sort 
   * @param comp new PointCompparer object to use
   * @param order order to sort by. Ascending or descending
   */
  public void selectionSortPoints(Customer[] customers,PointComparer comp,char order) {
    for (int i=0;i<customers.length;i++) {
      for (int j= i + 1; j<customers.length;j++) {
        if(comp.comparePoints(customers[j], customers[i], order)<0) {
          customers = swapCustomers(customers,i,j);
        }
      }
    }
    for(Customer customer : customers) {
      System.out.println(customer);
    }
  }

  /**
   * swaps a product with another in the list
   * @param products array of products
   * @param i index in array of first product
   * @param index index in array of second product
   * @return array with newly swapped products
   */
  public IProduct[] swap(IProduct[] products, int i, int index) {
    IProduct swappedProduct = products[i];
    products[i] = products[index];
    products[index] = swappedProduct;

    return products;
  }

  /**
   * Swaps customers in an array
   * @param customers array of customers to swap
   * @param i first index of customer
   * @param index second index of customer
   * @return new customers array with swapped customers
   */
  public Customer[] swapCustomers(Customer[] customers,int i,int index){
    Customer swappedCustomer = customers[i];
    customers[i] = customers[index];
    customers[index] = swappedCustomer;

    return customers;
  }

  /**
   * creates a new list of products with all the games
   * @return list of games
   * @throws IOException
   */
  public List<IProduct> getListOfGames() throws IOException {
    List<IProduct> gamesToReturn = new ArrayList<IProduct>();
    for (int i = 0; i < this.products.size(); i++) {
      if (this.products.get(i) instanceof Game) {
        gamesToReturn.add(this.products.get(i));
      }
    }
    return gamesToReturn;
  }

  /**
   * creates a new list of products with all the merchandise
   * @return list of merchandise 
   * @throws IOException
   */
  public List<IProduct> getListOfMerch() throws IOException {
    List<IProduct> merchToReturn = new ArrayList<IProduct>();
    for (int i = 0; i < this.products.size(); i++) {
      if (this.products.get(i) instanceof Merchandise) {
        merchToReturn.add(this.products.get(i));
      }
    }
    return merchToReturn;
  }

  /**
   * creates a new list of customers with all the customers
   * @return list of customers
   * @throws IOException
   */
  public List<Customer> getListOfCustomers() throws IOException {
    List<Customer> customerToReturn = new ArrayList<Customer>();
    for (int i =0;i<this.customers.size();i++) {
      customerToReturn.add(this.customers.get(i));
    }
    return customerToReturn;
  }

  /**
   * gets the list of products
   * @param filename filename to read data from
   * @return list of products
   * @throws IOException
   */
  public List<IProduct> getProducts(String filename) throws IOException {
    this.products = this.loader.loadProducts(filename);
    return this.products;
  }

  /**
   * gets the list of customers
   * @param filename filename to read data from
   * @return list of customers
   * @throws IOException
   */
  public List<Customer> getCustomers(String filename) throws IOException {
    this.customers = this.loader.loadCustomers(filename);
    return this.customers;
  }

  /**
   * sends a list of products to be added to a file
   * @param products list of products to be added to a file
   * @param filename file to be added to
   * @throws IOException
   */
  public void sendProducts(List<IProduct> products, String filename) throws IOException {
    for (int i = 0; i < products.size(); i++) {
      products.get(i).setIndex(i+1);
    }  
    
    this.loader.sendProducts(filename, products);
  }

  /**
   * sends a list of customers to be added to a file
   * @param customers list of customers to be added to a file
   * @param filename file to be added to
   * @throws IOException
   */
  public void sendCustomers(List<Customer> customers,String filename) throws IOException {
    for (int i=0;i<customers.size(); i++ ) {
      customers.get(i).setIndex(i+1);
    }
    this.loader.sendCustomers(filename,customers);
  }

  /**
   * add a new game
   * @param game new Game to add to products list
   */
  public void addNewGame(Game game) {
    this.products.add(game);
    System.out.println("\nGame added!");

    for (int i=(this.products.size())-1;i<this.products.size();i++) {
      System.out.println(this.products.get(i));
    }
  }

  /**
   * add a new merchandise
   * @param merch new Merchandise to add to products list
   */
  public void addNewMerchandise(Merchandise merch) {
    this.products.add(merch);
    System.out.println("\nMerchandise added!");

    for (int i=(this.products.size())-1;i<this.products.size();i++) {
      System.out.println(this.products.get(i));
    }
  }

  /**
   * adds a new customers
   * @param newCustomer new Customer to add to customers list
   */
  public void addNewCustomer(Customer newCustomer) {
    this.customers.add(newCustomer);
    System.out.println("\nCustomer added!");

    for(int i=(this.customers.size())-1;i<this.customers.size();i++) {
      System.out.println(this.customers.get(i));
    }
  }

  /**
   * updates quantity of a product and adds point to the customer ordering
   * @param productID id of product to order from
   * @param customerID id of customer ordering
   * @param products list of products to look through 
   * @param customers list of customers to look through
   * @param qtyOrdered how much the customer wants to order
   */
  public void addOrder(int productID, int customerID, List<IProduct> products, List<Customer> customers, int qtyOrdered) {
    this.products = products;
    this.customers = customers;
    int points = updatePoints(this.customers, customerID, qtyOrdered);
    int maxQuantity = this.products.get(productID).getQuantity();

    this.products.get(productID).setQuantity(maxQuantity-qtyOrdered);
    this.customers.get(customerID).setPoints(points);
  }

  public int updatePoints(List<Customer> customers, int customerID, int currQuantity) {
    int points = customers.get(customerID).getPoints();
    System.out.println("\nOld Points total: " + points);
    System.out.println("\nNew Points total: " + (points+(20*currQuantity)));

    return points+(20*currQuantity);
  }

  /**
   * delete a product from the list
   * @param index index of product to delete
   * @param products list of products to find product to delete
   */
  public void deleteProduct(int index, List<IProduct> products) {
    try {
      products.remove(index);
      this.products = products;

      for (int i=0;i<this.products.size();i++) {
        System.out.println(this.products.get(i));
      }
      System.out.println("\nProduct Deleted...");
    }
    catch (IndexOutOfBoundsException ioobe) {
      System.out.println("\nERROR 404: Product Not Found.\n");
    }
  }

  /**
   * delete a customer from the list
   * @param index index of customer to delete
   * @param customers list of customers to find customer to delete
   */
  public void deleteCustomer(int index, List<Customer> customers) {
    try {
      customers.remove(index);
      this.customers = customers;

      for (int i=0;i<this.customers.size();i++) {
        System.out.println(this.customers.get(i));
      }
      System.out.println("\nCustomers Deleted...");
    }
    catch (IndexOutOfBoundsException ioobe) {
      System.out.println("\nERROR 404: Customer Not Found.\n");
    }
  }

  /**
   * update a products data specifically 
   * @param productID id of product to update
   * @param infoToUpdate info of product to update
   */
  public void updateProduct(int productID, String[] infoToUpdate ) {
    try {
      if (infoToUpdate[0] == "Name") {
        this.products.get(productID).setName(infoToUpdate[1]);
      }
      else if (infoToUpdate[0] == "Creator") {
        this.products.get(productID).setCreator(infoToUpdate[1]);
      }
      else if (infoToUpdate[0] == "Price") {
        this.products.get(productID).setPrice(Double.parseDouble(infoToUpdate[1]));
      }
      else if (infoToUpdate[0] == "Discount") {
        this.products.get(productID).setDiscount(Double.parseDouble(infoToUpdate[1]));
      }
      else if (infoToUpdate[0] == "Size") {
        ((Merchandise)this.products.get(productID)).setSize(infoToUpdate[1].charAt(0));
      }
      else if (infoToUpdate[0] == "Genre") {
        ((Game)this.products.get(productID)).setGenre(infoToUpdate[1]);
      }
      else if (infoToUpdate[0] == "Rating") {
        ((Game)this.products.get(productID)).setRating(Double.parseDouble(infoToUpdate[1]));
      }
      else if (infoToUpdate[0] == "Quantity") {
        this.products.get(productID).setQuantity(Integer.parseInt(infoToUpdate[1]));
      }
      
      System.out.println("\nUpdated:\n" + this.products.get(productID));
      }
    catch (NumberFormatException nfe) {
      System.out.println("Invalid updated input! Cancelling update...\n");
    }
  }

  /**
   * updates a customers info specifically
   * @param customerID id of customer to update
   * @param infoToUpdate info to update for customer
   */
  public void updateCustomer(int customerID, String[] infoToUpdate) {
    try {
      if (infoToUpdate[0] == "Firstname") {
        this.customers.get(customerID).setFirstName(infoToUpdate[1]);
      }
      else if (infoToUpdate[0] == "Lastname") {
        this.customers.get(customerID).setLastName(infoToUpdate[1]);
      }
      System.out.println("\nUpdated:\n" + this.customers.get(customerID));
    }
    catch (NumberFormatException nfe) {
      System.out.println("Invalid updated input! Cancelling update...\n");
    }
  }

  
  /**
   * Creates a connection with an sql database
   * @param uName username
   * @param password password
   * @throws SQLException
   */
  public void getConnection(String uName, String password) throws SQLException{
    conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", uName, password);
  }

  /**
   * reads the customer data from the database and prints it
   * @throws SQLException
   */
  public void readCustomers () throws SQLException{
    ResultSet rs=null;
    String sql="{? = call java_reads.read_customers}";
    try(CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet)stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nID: " + rs.getInt("customer_id")+ "\nFirst Name: "+ rs.getString("firstname") + "\nLast name: "+ rs.getString("lastname")+ "\nPoints: " + rs.getInt("points"));
      }
    } catch(SQLException e) {
      e.printStackTrace();
    }
    catch(NullPointerException npe) {
      npe.printStackTrace();
    }
  }

  /**
   * reads the games data from the database and prints it
   * @throws SQLException
   */
  public void readGames() throws SQLException{
    ResultSet rs=null;
    String sql="{? = call java_reads.read_games}";
    try(CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet)stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nID: " + rs.getInt("game_id")+ "\nName: "+rs.getString("name") + "\nPrice: " + rs.getDouble("price")+"\nCreator: "+rs.getString("creator") + "\nDiscount: " + rs.getDouble("discount") + "\nRating: " + rs.getDouble("rating") + "\nQuantity: "+rs.getInt("quantity"));
      }
    } catch(SQLException e) {
      e.printStackTrace();
    }
    catch(NullPointerException npe) {
      npe.printStackTrace();
    }
  }

  /**
   * reads the merchandise data from the database and prints it
   * @throws SQLException
   */
  public void readMerch() throws SQLException{
    ResultSet rs=null;
    String sql="{? = call java_reads.read_merch}";
    try(CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet)stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nID: " + rs.getInt("merch_id")+"\nSize: " + rs.getString("merch_size") + "\nName: "+rs.getString("name")+"\nPrice: " + rs.getDouble("price") + "\nCreator: " +rs.getString("creator") +"\nDiscount: " +rs.getDouble("discount")+"\nQuantity: " + rs.getInt("quantity"));
      }
    } catch(SQLException e) {
      e.printStackTrace();
    }
    catch (NullPointerException npe) {
      npe.printStackTrace();
    }
  }

  /**
   * Prints all the data on the list
   * @param products a list of products with all the data to printed
   */
  public void showList(List<IProduct> products) {
    System.out.println("Showing all...");
    for (int i = 0; i < products.size(); i++) {
        System.out.println(products.get(i));
    }
  }

  /**
   * Prints all the data on the list
   * @param customers a list of customers with all the data to printed 
   */
  public void showListCustomers(List<Customer> customers) {
    for(int i=0;i<customers.size();i++) {
      System.out.println(customers.get(i));
    }
  }

  /**
   * Pre defined error message
   */
  public boolean errorMessage() {
    System.out.println("Invalid input detected! Please try again...\n");
    return true;
  }
}