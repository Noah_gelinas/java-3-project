package javaproject;

import product.*;
import java.util.*;
import java.io.*;
import customers.*;
import java.sql.SQLException;


/**
 * Employee class is the main application for employees
 * @author Noah Gelinas and Jaypee Tello
 * @version 11-25-2023
 */
public class EmployeeApplication {
  private static final Scanner SCAN = new Scanner(System.in); 
  /**
   * Main method to run application for admin
   * @throws IOException
   */
  public static void main(String[] args) throws IOException, SQLException {
    GameStore gamestore  = new GameStore();
    List<IProduct> products = gamestore.getProducts("../Products.csv");
    List<Customer> customers = gamestore.getCustomers("../Customers.csv");
    boolean exit = false;
    

    // DATABASE LOADER TO GET PRODUCTS FROM SQL INSTEAD OF CSV //
 /* boolean connects = false;
    while (!connects){
      System.out.println("Enter Username: ");
      String username = SCAN.next();
      String password = new String(System.console().readPassword("Password: "));
      try{
      gamestore.getConnection(username,password);
      connects=true;
      } catch (SQLException e) {
        e.printStackTrace();
        System.out.println("\nError in logging in! Would you like to try again? 1 = Yes | 2 = No.");
        int tryAgain = SCAN.nextInt();
        if (tryAgain==1) {
          connects=false;
        }else if(tryAgain==2) {
          System.exit(0);
        }
      } 
    } */

    while (!exit) {
      products = gamestore.getProducts("../Products.csv");
      customers = gamestore.getCustomers("../Customers.csv");
      char action = askCommandDisplayEmployee();
      char type = '\0';

      if (action == 'E'){
        System.out.println("Exiting...\n");
        exit = true;
      }
      else if (action == 'I'){
        System.out.println("Inserting...\n");
        char OrderOrCustomer = askOrderOrCustomer();
        
        if (OrderOrCustomer == 'C') {
          Customer customer = addCustomer(customers);
          gamestore.addNewCustomer(customer);
        }
        else {
          int productID = findProductID(products, action);
          int customerID = findCustomerID(customers, action);
          int qtyOrdered = updateQuantity(products, productID);
          gamestore.addOrder(productID, customerID, products, customers, qtyOrdered);
        }
      }
      else {
        type = askType();
      }

      if (action == 'R') {
        System.out.println("Reading...\n");
        char readType = specifyRead();
        List<IProduct> productsChosen = new ArrayList<IProduct>();
        List<Customer> customersChosen = new ArrayList<Customer>();

        if (type == 'G') {
          productsChosen = gamestore.getListOfGames();
          // gamestore.readGames(); // SQL
        }
        else if (type == 'M') {
          productsChosen = gamestore.getListOfMerch();
          // gamestore.readMerch(); // SQL
        }
        else {
          customersChosen = gamestore.getListOfCustomers();
          // gamestore.readCustomers(); // SQL
        }

        if (type == 'G' || type == 'M') {
          if (readType == 'A') {
            gamestore.showList(productsChosen);
          }
          else if (readType == 'O') {
            char[] order = askOrder(type);
            gamestore.orderRead(order, productsChosen);
          }
          else if (readType == 'S') {
            String[] specs = specificProduct(type);
            gamestore.showSpecificProducts(type, specs, productsChosen);
          }
        }
        else {
          if(readType == 'A') {
            gamestore.showListCustomers(customersChosen);
          }
          else if(readType=='O') {
            char order= askOrderPoints();
            gamestore.orderCustomersRead(order, customersChosen);
          }
          else if(readType=='S') {
            String customerID = String.valueOf(findCustomerID(customers, action)); 
            gamestore.showSpecificCustomer(customerID, customersChosen);
          }
        }
      }

      gamestore.sendProducts(products, "../Products.csv");
      gamestore.sendCustomers(customers,"../Customers.csv");
    }
  }

  /**
   * Asks the user what action they would like to perform
   * @return the action the user chose
   * @throws InputMismatchException
   * @throws StringIndexOutOfBoundsException
   */
  public static char askCommandDisplayEmployee() {
    boolean invalid = true;
    char action = '\0';

    while(invalid) {
      try {
        System.out.println("What do you want to do? Read = 'R' || Insert = 'I' || Exit = 'E' ");
        String input = SCAN.nextLine();
        action = input.charAt(0);

        if ((action == 'R' || action == 'E' || action == 'I') && input.length() == 1 ) {
          invalid = false;
        }
        else {
          System.out.println("\nOnly enter ('R') to Read || ('I') to Insert || ('E') to Exit !");
          invalid = true;
        }
      }
      catch (InputMismatchException ime) {
        invalid = errorMessage();
      }     
      catch (StringIndexOutOfBoundsException sioobe) {
        invalid = errorMessage();
      }   
    }
    return action;
  }

  /**
   * Asks user which product they would like to look for.
   * @return the product user has selected to search for
   * @throws InputMismatchException
   * @throws StringIndexOutOfBoundsException
   */
  public static char askType() {
    boolean invalid = true;
    char action = '\0';
    while (invalid) {
      try {
        System.out.println("Games ('G'), Merchandise ('M') or Customers ('C')?");
        String input = SCAN.nextLine();
        action = input.charAt(0);

        if ((action == 'G' || action == 'M' || action=='C') && input.length() == 1 ) {
          invalid = false;
        }
        else {
          System.out.println("\nOnly enter ('G') for Games, ('M') for Merchandise or ('C') for Customers!");
          invalid = true;
        }
      }
      catch (InputMismatchException ime) {
        errorMessage();
      }
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      } 
    }
    return action;
  }

  /**
   * Asks the user how they would like to receive the data
   * @return type of read they would like to perform
   * @throws InputMismatchException
   * @throws StringIndexOutOfBoundsException
   */
  public static char specifyRead() {
    char readType = '\0';
    boolean invalid = true;

    while(invalid) {
      try {
        System.out.println("By Specific ('S') || By Numerical Order ('O') || All ('A') ");
        String input = SCAN.nextLine();
        readType = input.charAt(0);

        if ((readType == 'S' || readType == 'O' || readType == 'A') && input.length() == 1 ) {
          invalid = false;
        }
        else {
          System.out.println("\nOnly enter ('S') to Specify Read || ('O') to Read by Order || ('A') to Read all !");
          invalid = true;
        }
      }
      catch (InputMismatchException ime) {
        errorMessage();
      }
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      }    
    }
    return readType;
  }

  /**
   * Asks the user how they would like the data ordered
   * @param productType game or merchandise the user is looking for 
   * @return the order selected
   * @throws InputMismatchException
   * @throws StringIndexOutOfBoundsException
   */
  public static char[] askOrder(char productType) {
    System.out.println("By Order...");
    char[] order = new char[2];
    boolean invalid = true;

    while(invalid) {
      try {
        System.out.println("By: Price ('P') || Rating ('R') || Discount ('C') || Quantity ('Q') ? ");
        String input1 = SCAN.nextLine();
        order[0] = input1.charAt(0);

        System.out.println("In what order? Ascending ('A') or Descending ('D') ? ");
        String input2 = SCAN.nextLine();
        order[1] = input2.charAt(0);

        if ((order[1] == 'A' || order[1] == 'D') && (order[0] == 'P' || (order[0] == 'R' && productType == 'G') || order[0] == 'C' || order[0] == 'Q') && input1.length() == 1 && input2.length() == 1) {
          invalid = false;
        }
        else if (order[0] == 'R' && productType == 'M'){
          System.out.println("\nNo ratings available for merchandise! Please try again.");
        }
        else {
          errorMessage();
        }
      }
      catch (InputMismatchException ime) {
        errorMessage();
      }   
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      } 
    }
    return order;
  } 

  /**
   * Asks the user what specific field they want to search by then asks them to enter it
   * @param productType Merchandise or Game has the user selected to search by.
   * @return array with what user searched by and what they entered
   * @throws InputMismatchException
   * @throws StringIndexOutOfBoundsException
   */
  public static String[] specificProduct(char productType) {
    System.out.println("By Specific...");
    String[] specific = new String[2];
    char c = '\0';
    String input = "";
    boolean invalid = true;
    String changeFind = "";
    

    while(invalid) {
      boolean isEnterKey = true;
      try {
        if (productType == 'M') {
          changeFind = "|| Size ('S') ";
        }
        else {
          changeFind = "|| Genre ('G') ";
        }

        System.out.println("Find: Creator ('C') " + changeFind + "|| Name ('N') || ID ('I') ?");
        input = SCAN.nextLine();
        c = input.charAt(0);
        
        while (isEnterKey) {
          if(c=='C') {
            System.out.println("Enter specific Creator: ");
            specific[1] = SCAN.nextLine();
            
            if (specific[1].equals("")){
              invalid = errorMessage();
              isEnterKey=true;
            }else{ 
              isEnterKey=false;
            }

          }else if(c=='G') {
            System.out.println("Enter specific Genre: ");
            specific[1] = SCAN.nextLine();
            if (specific[1].equals("")){
              invalid = errorMessage();
              isEnterKey=true;
            }else{ 
              isEnterKey=false;
            }
          }else if(c=='N'){
            System.out.println("Enter specific Name: ");
            specific[1] = SCAN.nextLine();
            if (specific[1].equals("")){
              invalid = errorMessage();
              isEnterKey=true;
            }else{ 
              isEnterKey=false;
            }
          }else if(c=='I') {
            System.out.println("Enter specific ID: ");
            specific[1] = SCAN.nextLine();
            if (specific[1].equals("")){
              invalid = errorMessage();
              isEnterKey=true;
            }else{ 
              isEnterKey=false;
            }
          }else if(c=='S') {
            System.out.println("Enter specific Size (S/M/L): ");
            specific[1] = SCAN.nextLine();
            if (specific[1].equals("")){
              invalid = errorMessage();
              isEnterKey=true;
            }else{ 
              isEnterKey=false;
            }
          }
          else {
            isEnterKey=false;
          }
        }

        if ((c == 'C' || c == 'G' || c == 'N' || c == 'I' ) && input.length() == 1) {
          if (c == 'C') {
            specific[0] = "Creator";
          }
          else if (c == 'G') {
            specific[0] = "Genre";
          }
          else if (c == 'N') {
            specific[0] = "Name";
          }
          else if (c == 'I') {
            specific[0] = "Index";
          }
          invalid = false;
        }
        else if ((c == 'S' && productType == 'M') && (specific[1].equals("M") || specific[1].equals("S")  || specific[1].equals("L") )) {
          specific[0] = "Size";
          invalid = false;
        }
        else {
          invalid = errorMessage();
        }
      }
      catch (InputMismatchException ime) {
        invalid = errorMessage();
      }   
      catch (StringIndexOutOfBoundsException sioobe) {
        invalid = errorMessage();
      } 
    }
    return specific;
  }

  /**
   * Pre defined error message
   */
  public static boolean errorMessage() {
    System.out.println("Invalid input detected! Please try again...\n");
    return true;
  }

  /**
   * Asks the user if they want to insert a new customer or order.
   * @return what the user decided. Either customer or order to insert
   */
  public static char askOrderOrCustomer() {
    boolean invalid = true;
    char whatToInsert='\0';
    
    while(invalid) {
      try {
        System.out.println("\nInsert new Customer ('C') or Order ('O')?");
        String input = SCAN.nextLine();
        whatToInsert = input.charAt(0);
        if(whatToInsert == 'C' || whatToInsert== 'O' && input.length()== 1) {
          invalid = false;
        }
        else{
          System.out.println("\nOnly enter ('C') for new Customer or ('O') for new Order!");
          invalid = true;
        }
      }
      catch (InputMismatchException ime) {
        errorMessage();
      }
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      }
    }
    return whatToInsert;
  }

  /**
   * creates a new customer object to add to the list of customer
   * @param customers List of existing customers
   * @return the new customer object to add to the list of customers
   */
  public static Customer addCustomer(List<Customer> customers) {
    boolean invalid = true;
    String firstname="";
    String lastname="";
    int points=0;
    boolean isNegative=true;   
    int ID = 0;
    Customer newCustomer = new Customer(ID, firstname, lastname, points);
    System.out.println("Preparing to add customer...");

    while (invalid) {
      try {
        System.out.println("\nEnter Firstname: ");
        firstname=SCAN.nextLine();

        System.out.println("\nEnter Lastname: ");
        lastname=SCAN.nextLine();

        while(isNegative) {
          System.out.println("\nEnter Points: ");
          points = SCAN.nextInt();
          SCAN.nextLine();

          if (points < 0) {
          errorMessage();
          }else{
            isNegative=false;
          }
        }

        ID = customers.size() + 1;
        
        invalid=false;
        if (firstname == "" || lastname == "") {
          System.out.println("Empty fields detected! Please try again...");
          invalid = true;
        }
        else  {
          newCustomer = new Customer(ID,firstname,lastname,points);
          System.out.println("\nCustomer added!");
        }
      } catch (InputMismatchException ime) {
        errorMessage();
      }
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      } 
      catch (NumberFormatException nfe) {
        errorMessage();
      }
    }
    return newCustomer;
  }

  /**
   * Updates the quantity a product has left after an order.
   * @param products List of products
   * @param productID id of product to update
   * @return quantity user decided to order
   */
  public static int updateQuantity(List<IProduct> products,int productID) {
    boolean invalid=true;
    int maxQuantity=0;
    int quantity=0;
    while(invalid) {
      try{
        maxQuantity = products.get(productID).getQuantity();

        System.out.println("\nHow many would you like to order? Max: " + maxQuantity + " Min: 0");
        String input = SCAN.nextLine();
        quantity = Integer.parseInt(input);

        invalid = false;
        if(quantity > maxQuantity || quantity < 0) {
          invalid = errorMessage();
        }
      }
      catch (InputMismatchException ime) {
        invalid = errorMessage();
      }
      catch (IndexOutOfBoundsException ioobe) {
        invalid = errorMessage();
      }
      catch (NumberFormatException nfe) {
        invalid = errorMessage();
      }
    }
    System.out.println("\nOld Quantity: " + maxQuantity);
    System.out.println("\nNew Quantity: " + (maxQuantity-quantity));
    return quantity;
  }

  /**
   * Lets the user delete a product of the list
   * @param products List of IProducts to delete a product fromm
   * @return List of IProducts with one less product which is the product the user defined
   */
  public static int findProductID(List<IProduct> products, char action) {
    boolean invalid = true;
    int index = 0;
    String deleteDisplay = "Preparing to delete a product...\nEnter ID of product to be deleted: ";
    String updateDisplay = "Preparing to update a product...\nEnter ID of product you want to update: ";
    String specificDisplay = "Enter specific product ID of order: ";

    while (invalid) {
      try {    
        if (action == 'U') {
          System.out.println(updateDisplay);
        }
        else if (action == 'D') {
          System.out.println(deleteDisplay);
        }
        else {
          System.out.println(specificDisplay);
        }

        String input = SCAN.nextLine();
        index = Integer.parseInt(input);

        products.get(index-1); // TEST TO CHECK IF PRODUCT WITH PRODUCT ID EXIST 
        invalid = false;
      }
      catch (NumberFormatException nfe) {
        invalid = errorMessage();
      }
      catch (IndexOutOfBoundsException ioobe) {
        invalid = errorMessage();
      }
    }
    return index - 1;
  }

  /**
   * finds the customer representing users selected customer id
   * @param customers list of customers
   * @param action what user wants to do
   * @return customer id
   */
  public static int findCustomerID(List<Customer> customers, char action) {
    boolean invalid = true;
    int index = 0;
    String deleteDisplay = "Preparing to delete a customer...\nEnter ID of customer to be deleted: ";
    String updateDisplay = "Preparing to update a customer...\nEnter ID of customer you want to update: ";
    String specificDisplay = "Enter specific customer ID of orderer: ";
    while (invalid) {
      try {
        
        if (action == 'U' || action == 'D') {
          for (int i=0;i < customers.size();i++) {
            System.out.println(customers.get(i));
          }
        }

        if (action == 'U') {
          System.out.println(updateDisplay);
        }
        else if (action == 'D') {
          System.out.println(deleteDisplay);
        }
        else {
          System.out.println(specificDisplay);
        }

        String input = SCAN.nextLine();
        index = Integer.parseInt(input);
        
        customers.get(index-1); // TEST TO CHECK IF PRODUCT WITH PRODUCT ID EXIST 
        invalid = false;
      }
      catch (NumberFormatException nfe) {
        invalid = errorMessage();
      }
      catch (IndexOutOfBoundsException ioobe) {
        invalid = errorMessage();
      }
    }
    return index - 1;
  }

  /**
   * Asks the user in what order they would like to receive their data based on points
   * @return either ascending or descending order
   */
  public static char askOrderPoints() {
    System.out.println("\n*Will be ordered by points.");
    char order='\0';
    boolean invalid = true;

    while(invalid) {
      try {
        System.out.println("In what order? Ascending ('A') or Descending ('D') ? ");
        String input = SCAN.nextLine();
        order = input.charAt(0);

        if ((order == 'A' || order == 'D') && input.length() == 1) {
          invalid = false;
        }
        else {
          errorMessage();
        }
      }
      catch (InputMismatchException ime) {
        errorMessage();
      }   
      catch (StringIndexOutOfBoundsException sioobe) {
        errorMessage();
      } 
    }
    return order;
  } 
}
