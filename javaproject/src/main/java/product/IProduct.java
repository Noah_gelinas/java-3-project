package product;

/**
 * IProduct is an interface which defines the following methods and is implemented by Merchandise and Game
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public interface IProduct {
  /**
   * returns the name of specific product
   * @return name of product
   */
  String getName();

  /**
   * returns the creator of specific product
   * @return creator of product
   */
  String getCreator();

  /**
   * returns the price of specific product
   * @return price of product
   */
  double getPrice();

  /**
   * returns the discount of specific product
   * @return discount of product
   */
  double getDiscount();

  /**
   * returns the index of specific product
   * @return index of product
   */
  int getIndex();

  /**
   * returns the data of this product
   * @return a string of all the data from this product
   */
  String getProductData();

  /**
   * sets the new name of specific product
   * @param name new name of product
   */
  public void setName(String name);

  /**
   * sets the new price of specific product
   * @param price new price of product
   */
  public void setPrice(double price); 

  /**
   * sets the new creator of specific product
   * @param creator new creator of product
   */
  public void setCreator(String creator);

   /**
   * sets the new discount of specific product
   * @param discount new discount of product
   */
  public void setDiscount(double discount);

  /**
   * sets the new index of specific product
   * @param index new index of product
   */
  public void setIndex(int index);

  /**
   * gets the index of a specific product
   * @return the index of the product
   */
  public int getQuantity();

  /**
   * sets the quantity of specific product
   * @param quantity the new quantity of the product
   */
  public void setQuantity(int quantity);

}
