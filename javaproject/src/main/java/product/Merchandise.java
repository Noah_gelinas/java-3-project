package product;

/**
 * Merchandise is a class which creates the merchandise product
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class Merchandise implements IProduct {
    private char size;
    private String name;
    private double price;
    private String creator;
    private double discount;
    private int index;
    private int quantity;

  /**
   * Merchandise constructor which initializes a new merchandise object
   * @param index id of new merch in list
   * @param size size of new merch
   * @param name name of new merch 
   * @param price price of new merch
   * @param creator creator of new merch
   * @param discount discount of new merch
   */
    public Merchandise(int index,char size, String name,double price,String creator,double discount, int quantity) {
      this.index = index;
      this.size=size;
      this.name=name;
      this.price=price;
      this.creator=creator;
      this.discount=discount;
      this.quantity=quantity;
    }

  /**
   * returns the name of specific merch
   * @return name of merch
   */
    public String getName() {
      return this.name;
    }

  /**
   * returns the creator of specific merch
   * @return creator of merch
   */
    public String getCreator() {
      return this.creator;
    }

  /**
   * returns the price of specific merch
   * @return price of merch
   */
    public double getPrice() {
      return this.price;
    }

    /**
     * returns the size of specific merch
     * @return size of merch
     */
    public char getSize() {
      return this.size;
    }

  /**
   * returns the discount of specific merch
   * @return discount of merch
   */
    public double getDiscount() {
      return this.discount;
    }

  /**
   * returns the index of specific merch
   * @return index of merch
   */
    public int getIndex() {
      return this.index;
    }
  
  /**
   * returns the quantity of the game
   * @return quantity of the game
   */
  public int getQuantity() {
    return this.quantity;
  }

  /**
   * sets the new name of specific merch
   * @param name new name of merch
   */
    public void setName(String name) {
      this.name = name;
    }
  
  /**
   * sets the new price of specific merch
   * @param price new price of merch
   */
    public void setPrice(double price) {
      this.price = price;
    }
  
  /**
   * sets the new creator of specific merch
   * @param creator new creator of merch
   */
    public void setCreator(String creator) {
      this.creator = creator;
    }
  
  /**
   * sets the new discount of specific merch
   * @param discount new discount of merch
   */
    public void setDiscount(double discount) {
      this.discount = discount;
    }

    /**
   * sets the new size of specific merch
   * @param size new size of merch
   */
    public void setSize(char size) {
      this.size = size;
    }
    
    public void setIndex(int index) {
      this.index = index;
    }

  /**
   * sets the new quantity of specific game
   * @param quantity new quantity of game
   */
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  /**
   * returns the data of this merch
   * @return a string of all the data from this merch
   */
    public String getProductData() {
      String productData = (this.index + "," + this.size + "," + this.name + "," + this.price + "," + this.creator + "," + this.discount + "," + this.quantity);
      return productData;
    }

  /**
   * Overrides the toString to print all the data with identifiers
   * @return a string of all the data with its identifiers of this merch
   */
    @Override
    public String toString() {
      return ("\nID: " +this.index+"\nSize: " + this.size + "\nName: " + this.name+ "\nPrice: $" + this.price + "\nCreator: " + this.creator + "\nDiscount: " + this.discount + "\nQuantity: " + this.quantity + "\n");
    }
}
