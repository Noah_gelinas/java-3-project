package product;

/**
 * Game is a class which creates the Game product
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class Game implements IProduct {
  private String name;
  private double price;
  private String creator;
  private double discount;
  private String genre;
  private double rating;
  private int index;
  private int quantity;

  /**
   * Game constructor which initializes a new game object
   * @param index id of new game in list
   * @param name name of new game 
   * @param price price of new game
   * @param creator creator of new game
   * @param discount discount of new game
   * @param genre genre of new game
   * @param rating rating of new game
   * @param quantity quantity of the game
   */
  public Game (int index, String name, double price, String creator, double discount, String genre, double rating, int quantity) {
    this.index = index;
    this.name = name;
    this.price = price;
    this.creator = creator;
    this.discount = discount;
    this.genre = genre;
    this.rating = rating;
    this.quantity = quantity;
  }

  /**
   * returns the name of specific game
   * @return name of game
   */
  public String getName() {
    return this.name;
  }

  /**
   * returns the price of specific game
   * @return price of game
   */
  public double getPrice() {
    return this.price;
  }

  /**
   * returns the creator of specific game
   * @return creator of game
   */
  public String getCreator() {
    return this.creator;
  }

  /**
   * returns the discount of specific game
   * @return discount of game
   */
  public double getDiscount() {
    return this.discount;
  }

  /**
   * returns the genre of specific game
   * @return genre of game
   */
  public String getGenre() {
    return this.genre;
  }

  /**
   * returns the rating of specific game
   * @return rating of game
   */
  public double getRating() {
    return this.rating;
  }

  /**
   * returns the index of specific game
   * @return index of game
   */
  public int getIndex() {
    return this.index;
  }
  
  /**
   * returns the quantity of the game
   * @return quantity of the game
   */
  public int getQuantity() {
    return this.quantity;
  }

  /**
   * sets the new name of specific game
   * @param name new name of game
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * sets the new price of specific game
   * @param price new price of game
   */
  public void setPrice(double price) {
    this.price = price;
  }

  /**
   * sets the new creator of specific game
   * @param creator new creator of game
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * sets the new discount of specific game
   * @param discount new discount of game
   */
  public void setDiscount(double discount) {
    this.discount = discount;
  }

  /**
   * sets the new genre of specific game
   * @param genre new genre of game
   */
  public void setGenre(String genre) {
    this.genre = genre;
  }

  /**
   * sets the new rating of specific game
   * @param rating new rating of game
   */
  public void setRating(double rating) {
    this.rating = rating;
  }

  /**
   * sets the new index of specific game
   * @param index new index of game
   */
  public void setIndex(int index) {
    this.index = index;
  }

  /**
   * sets the new quantity of specific game
   * @param quantity new quantity of game
   */
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
  
  /**
   * returns the data of this game
   * @return a string of all the data from this game
   */
  public String getProductData() {
    String productData = (this.index + "," + this.name + "," + this.price + "," + this.creator + "," + this.discount + "," + this.genre + "," + this.rating + "," + this.quantity);
    return productData;
  }

  /**
   * Overrides the toString to print all the data with identifiers
   * @return a string of all the data with its identifiers of this game
   */
  @Override
  public String toString() {
    return ("\nID: " + this.index + "\nName: " + this.name + "\nPrice: $" + this.price + "\nCreator: " + this.creator + "\nDiscount: " + this.discount + "\nGenre: " + this.genre + "\nRating: " + this.rating + "\nQuantity: " + this.quantity + "\n");
  }
}
