package comparer;

import product.IProduct;

/**
 * PriceComparer is a class used for comparing two products by their prices in either ascending or descending order
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class PriceComparer implements IProductComparer {

  /**
 * compares two products prices by either ascending or descending
 * @param one first product to compare
 * @param two second product to compare
 * @param order either ascending or descending
 * @return number representing which product is greater.
 */
  public double compareProducts(IProduct one, IProduct two, char order) {
    if (order == 'A') {
      return (one.getPrice() - two.getPrice());
    }
    else {
      return (two.getPrice() - one.getPrice());
    }
  }
}
