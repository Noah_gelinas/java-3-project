package comparer;

import product.IProduct;

/**
 * QuantityComparer is a class used for comparing two products by their quantities in either ascending or descending order
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class QuantityComparer implements IProductComparer {
  /**
   * compares two products prices by either ascending or descending
   * @param one first product to compare
   * @param two second product to compare
   * @param order either ascending or descending
   * @return number representing which product is greater.
   */
  public double compareProducts(IProduct one, IProduct two, char order) {
    if (order == 'A') {
      return (one.getQuantity() - two.getQuantity());
    }
    else {
      return (two.getQuantity() - one.getQuantity());
    }
  }
}
