package comparer;

import product.IProduct;
import product.Game;

/**
 * RatingComparer is a class used for comparing two products by their ratings in either ascending or descending order
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class RatingComparer implements IProductComparer {

  /**
 * compares two products ratings by either ascending or descending
 * @param one first product to compare
 * @param two second product to compare
 * @param order either ascending or descending
 * @return number representing which product is greater.
 */
  public double compareProducts(IProduct one, IProduct two, char order) {
    if (order == 'A') {
      return (((Game)one).getRating() - ((Game)two).getRating());
    }
    else {
      return (((Game)two).getRating() - ((Game)one).getRating());
    }
  }
}
