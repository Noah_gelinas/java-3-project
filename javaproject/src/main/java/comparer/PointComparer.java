package comparer;

import customers.*;

/**
 * PointComparer is a class used for comparing two customers by their point totals in either ascending or descending order
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-29-2023
 */
public class PointComparer {

    /**
     * comparePoints compares two customers points to find which one is bigger then the other
     * @param one first instance of Customer object to compare
     * @param two second instace of Customer object to compare
     * @param order the order in which to compare for either ascending or descending
     * @return number representing which number is higher
     */
    public int comparePoints(Customer one,Customer two,char order) {
      if (order == 'A') {
          return (one.getPoints() - two.getPoints());
      }
      else {
          return (two.getPoints() - one.getPoints());
      }
    }
}
