package comparer;

import product.IProduct;
/**
 * IProductComparer is an interface which defines the following method used in the implemented classes: DiscountComparer, PriceComparer, and RatingComparer
 */
public interface IProductComparer {

  /**
   * compares two products by either ascending or descending
   * @param one first product to compare
   * @param two second product to compare
   * @param order either ascending or descending
   * @return number representing which product is greater.
   */
  double compareProducts(IProduct one, IProduct two, char order);
}