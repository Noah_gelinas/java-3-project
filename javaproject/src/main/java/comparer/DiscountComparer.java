package comparer;

import product.IProduct;

/**
 * DiscountComparer is a class used for comparing two products by their discount in either ascending or descending order
 * @author Jaypee Tello and Noah Gelinas
 * @version 11-25-2023
 */
public class DiscountComparer implements IProductComparer {

/**
 * compares two products discounts by either ascending or descending
 * @param one first product to compare
 * @param two second product to compare
 * @param order either ascending or descending
 * @return number representing which product is greater.
 */
  public double compareProducts(IProduct one, IProduct two, char order) {
    if (order == 'A') {
      return (one.getDiscount() - two.getDiscount());
    }
    else {
      return (two.getDiscount() - one.getDiscount());
    }
  }
}
