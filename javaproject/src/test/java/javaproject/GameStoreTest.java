package javaproject;
import static org.junit.Assert.*;

import org.junit.Test;

import product.*;
import java.util.*;

import java.io.*;
import java.sql.*;
import customers.Customer;

public class GameStoreTest {
  GameStore gamestore;
  List<IProduct> products;
  Merchandise merch;
  List<Customer> customers;
  Customer customer;

  public GameStoreTest() throws IOException {
    this.gamestore = new GameStore();
    this.products = new ArrayList<IProduct>();
    this.merch = new Merchandise(1, 'L', "Bag", 20, "Louis", 10, 200);
    this.products.add(merch);
    this.customers = new ArrayList<Customer>();
    this.customer = new Customer(1, "Johnny", "Baker", 200);
    this.customers.add(customer);
  }

  @Test
  public void updateCustomerTest() throws IOException {
    int customerID = 0;
    String[] infoToUpdate = new String[2];
    infoToUpdate[0] = "Firstname";
    infoToUpdate[1] = "Alice";
    this.customers = gamestore.getCustomers("../Customers.csv");
    gamestore.updateCustomer(customerID, infoToUpdate);
    
    assertEquals("Alice", this.customers.get(customerID).getFirstName());
  }

  @Test
  public void updateProductTest() throws IOException {
    int productID = 0;
    String[] infoToUpdate = new String[2];
    infoToUpdate[0] = "Name";
    infoToUpdate[1] = "Left 4 Dead";
    this.products = gamestore.getProducts("../Products.csv");
    gamestore.updateProduct(productID, infoToUpdate);
    
    assertEquals("Left 4 Dead", this.products.get(productID).getName());
  }

  @Test 
  public void deleteCustomerTest() throws IOException {
    int index = 0;
    this.customers = gamestore.getCustomers("../Customers.csv");

    gamestore.deleteCustomer(index, this.customers);
    List<Customer> customersTest = new ArrayList<Customer>();
    customersTest = gamestore.getCustomers("../Customers.csv");
    customersTest.remove(0);

    assertEquals(this.customers.toString(), customersTest.toString());
  }

  @Test 
  public void deleteTest() throws IOException {
    int index = 0;
    this.products = gamestore.getProducts("../Products.csv");

    gamestore.deleteProduct(index, this.products);
    List<IProduct> productsTest = new ArrayList<IProduct>();
    productsTest = gamestore.getProducts("../Products.csv");
    productsTest.remove(0);

    assertEquals(this.products.toString(), productsTest.toString());
  }

  @Test
  public void swapTest() {
    IProduct[] products = new Game[2];
    products[0]= new Game(1,"test1",100,"tester",0,"testing",5,30);
    products[1]= new Game(2,"test2",150,"tester2",0,"testing2",4,40);

    IProduct[] newProducts = new Game[2];
    newProducts[1] = new Game(1,"test1",100,"tester",0,"testing",5,30);
    newProducts[0] = new Game(2,"test2",150,"tester2",0,"testing2",4,40);

    IProduct[] received = new Game[2];
    received=gamestore.swap(products, 0, 1);
    assertEquals(newProducts[0].toString(), received[0].toString());
    assertEquals(newProducts[1].toString(), received[1].toString());
  }
  
  @Test
  public void swapCustomerTest() {
    Customer[] customers = new Customer[2];
    customers[0] = new Customer(1, "Noah", "Gelinas", 20);
    customers[1] = new Customer(2, "Jaypee", "Tello", 40);

    Customer[] newCustomers= new Customer[2];
    newCustomers[1] = new Customer(1, "Noah", "Gelinas", 20);
    newCustomers[0] = new Customer(2, "Jaypee", "Tello", 40);

    Customer[] received= new Customer[2];
    received=gamestore.swapCustomers(customers, 0, 1);
    assertEquals(newCustomers[0].toString(),received[0].toString());
    assertEquals(newCustomers[1].toString(),received[1].toString());
  }

  @Test
  public void getListOfCustomersTest() {
    List<Customer> customerToReturn = new ArrayList<Customer>();
    for (int i =0;i<this.customers.size();i++) {
      customerToReturn.add(this.customers.get(i));
    }
    assertEquals(customerToReturn.get(0).toString(), this.customers.get(0).toString());
  }

  @Test
  public void getListOfGamesTest() {
    List<IProduct> gamesToReturn = new ArrayList<IProduct>();
    List<IProduct> games = new ArrayList<IProduct>();
    games.add(new Game(1,"Test",10,"testers",0,"testing",3,100));
    for (int i = 0;i < games.size();i++) {
      if(games.get(i) instanceof Game) {
        gamesToReturn.add(games.get(i));
      }
    } 
    assertEquals(gamesToReturn.get(0).toString(), games.get(0).toString());
  }

  @Test
  public void getListOfMerchTest() {
    List<IProduct> merchToReturn = new ArrayList<IProduct>();
    List<IProduct> merch =new ArrayList<IProduct>();
    merch.add(new Merchandise(1, 'M', "test", 100, "testing", 0, 100));

    for (int i = 0; i < merch.size();i++) {
      if(merch.get(i) instanceof Merchandise) {
        merchToReturn.add(merch.get(i));
      }
    }
    assertEquals(merchToReturn.get(0).toString(), merch.get(0).toString());
  }

  @Test
  public void getProductsTest() throws IOException {
    String filename="../Products.csv";
    List<IProduct> product = new ArrayList<IProduct>();
    product.add(new Game(1,"Fortnite",29.99,"Epic Games",0,"Battle Royale",4.8,500));
    assertEquals(product.get(0).toString(), gamestore.getProducts(filename).get(0).toString());
  }

  @Test 
  public void errorMessageTest() {
    assertEquals(true, this.gamestore.errorMessage());
  }

  @Test
  public void showListTest() {
    gamestore.showList(this.products);
  }

  @Test
  public void readMerchTest() throws SQLException {
    gamestore.readMerch();
  }

  @Test
  public void readGamesTest() throws SQLException {
    gamestore.readGames();
  }

  @Test
  public void readCustomersTest() throws SQLException {
    gamestore.readCustomers();
  }

  @Test
  public void getCustomersTest() throws IOException {
    String filename="../Customers.csv";
    List<Customer> customer = new ArrayList<Customer>();
    customer.add(new Customer(1,"Noah","Gelinas", 16100));
    assertEquals(customer.get(0).toString(), gamestore.getCustomers(filename).get(0).toString());
  }

  @Test
  public void updatePointsTest() {
    List<Customer> customer = new ArrayList<Customer>();
    customer.add(new Customer(1,"Noah","Gelinas", 16100));
    int points =(customer.get(0).getPoints())+200;
    assertEquals(points, gamestore.updatePoints(customer, 0, 10));
  }
}
