package comparer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import product.Game;
import product.IProduct;

public  class QuantityComparerTest {
  IProductComparer comp = new QuantityComparer();
  double delta = .0000001;
  
  @Test
  public void compareProductsAscendingTest() {
      IProduct[] products = new IProduct[2];
      products[0]=new Game(1,"test",10.0,"epic games", 3.0,"battle royale",4.0, 200);
      products[1]=new Game(2,"test2",11.0,"epic games", 15.0, "shooter", 2.0, 100);
      char order ='A';
      double expected = 100;
      assertEquals(expected,comp.compareProducts(products[0], products[1], order),delta);
  }

  @Test
  public void compareProductsDescendingTest () {
      IProduct[] products = new IProduct [2];
      products[0]=new Game(1,"test",10.0,"epic games", 3.0,"battle royale",4.0, 200);
      products[1]=new Game(2,"test2",11.0,"epic games", 15.0, "shooter", 2.0, 100);
      char order ='D';
      double expected = -100;
      assertEquals(expected,comp.compareProducts(products[0], products[1], order),delta);
  }
}
