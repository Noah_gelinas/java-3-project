package comparer;
import static org.junit.Assert.*;

import org.junit.Test;

import customers.*;
public class PointComparerTest {
    PointComparer comp =new PointComparer();

    @Test
    public void comparePointsAscendingTest () {
        Customer[] customers = new Customer[2];
        customers[0]=new Customer(1,"Noah","Gelinas",1000);
        customers[1]=new Customer(1,"Jaypee","Tello",500);
        char order='A';
        int expected=500;
        assertEquals(expected,comp.comparePoints(customers[0],customers[1],order));
    }

    @Test
    public void comparePointsDescendingTest () {
        Customer[] customers = new Customer[2];
        customers[0]=new Customer(1,"Noah","Gelinas",1000);
        customers[1]=new Customer(1,"Jaypee","Tello",500);
        char order='D';
        int expected=-500;
        assertEquals(expected,comp.comparePoints(customers[0],customers[1],order));
    }

}
