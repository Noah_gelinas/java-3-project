package product;
import static org.junit.Assert.*;

import org.junit.Test;


public class MerchandiseTest {
  private final Merchandise MerchandiseTEST = new Merchandise(1, 'S',"Shirt",12.99,"Kine", 20, 200);
  private final double DELTA = 1e-15;

  @Test
  public void getSizeTest() {
    assertEquals('S', MerchandiseTEST.getSize());
  }

  @Test
  public void getIndexTest() {
    assertEquals(1, MerchandiseTEST.getIndex());
  }
  
  @Test
  public void getNameTest() {
    assertEquals("Shirt", MerchandiseTEST.getName());
  }

  @Test
  public void getPriceTest() {
    assertEquals(12.99, MerchandiseTEST.getPrice(), DELTA);
  }

  @Test
  public void getCreatorTest() {
    assertEquals("Kine", MerchandiseTEST.getCreator());
  }

  @Test
  public void getDiscountTest() {
    assertEquals(20, MerchandiseTEST.getDiscount(), DELTA);
  }

  @Test
  public void getQuantityTest() {
    assertEquals(200, MerchandiseTEST.getQuantity(), DELTA);
  }

  @Test
  public void setSizeTest() {
    MerchandiseTEST.setSize('L');
    assertEquals('L', MerchandiseTEST.getSize());
  }
  
  @Test
  public void setNameTest() {
    MerchandiseTEST.setName("Jacket");
    assertEquals("Jacket", MerchandiseTEST.getName());
  }

  @Test
  public void setPriceTest() {
    MerchandiseTEST.setPrice(19.99);
    assertEquals(19.99, MerchandiseTEST.getPrice(), DELTA);
  }

  @Test
  public void setCreatorTest() {
    MerchandiseTEST.setCreator("Logo");
    assertEquals("Logo", MerchandiseTEST.getCreator());
  }

  @Test
  public void setDiscountTest() {
    MerchandiseTEST.setDiscount(35);
    assertEquals(35, MerchandiseTEST.getDiscount(), DELTA);
  }

  @Test
  public void setQuantityTest() {
    MerchandiseTEST.setQuantity(400);
    assertEquals(400, MerchandiseTEST.getQuantity(), DELTA);
  }

  @Test
  public void setIndexTest() {
    MerchandiseTEST.setIndex(5);
    assertEquals(5, MerchandiseTEST.getIndex());
  }

  @Test
  public void toStringTest() {
    String expected = "\nID: 1\nSize: S\nName: Shirt\nPrice: $12.99\nCreator: Kine\nDiscount: 20.0\nQuantity: 200\n";
    assertEquals(expected, MerchandiseTEST.toString());
  }
}
