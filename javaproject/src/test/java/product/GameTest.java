package product;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest
{
    private final Game GAMETEST = new Game(1, "Forknight", 15.99, "Ipec", 15.0, "FPS", 5.00, 200);
    private final double DELTA = 1e-15;

    @Test
    public void getNameTest() {
        assertEquals("Forknight", GAMETEST.getName());
    }

    @Test
    public void getIDTest() {
        assertEquals(1,GAMETEST.getIndex());
    }
    @Test
    public void getPriceTest() {
        assertEquals(15.99, GAMETEST.getPrice(), DELTA);
    }

    @Test
    public void getCreatorTest() {
        assertEquals("Ipec", GAMETEST.getCreator());
    }

    @Test
    public void getDiscountTest() {
        assertEquals(15, GAMETEST.getDiscount(), DELTA);
    }

    @Test
    public void getGenreTest() {
        assertEquals("FPS", GAMETEST.getGenre());
    }

    @Test
    public void getRatingTest() {
        assertEquals(5.00, GAMETEST.getRating(), DELTA);
    }

    @Test
    public void getQuantityTest() {
        assertEquals(200, GAMETEST.getQuantity());
    }

    @Test
    public void setNameTest() {
        GAMETEST.setName("Fortnayt");
        assertEquals("Fortnayt", GAMETEST.getName());
    }

    @Test
    public void setIDTest() {
        GAMETEST.setIndex(2);
        assertEquals(2,GAMETEST.getIndex());
    }
    @Test
    public void setPriceTest() {
        GAMETEST.setPrice(200);
        assertEquals(200.00, GAMETEST.getPrice(), DELTA);
    }

    @Test
    public void setCreatorTest() {
        GAMETEST.setCreator("Me");
        assertEquals("Me", GAMETEST.getCreator());
    }

    @Test
    public void setDiscountTest() {
        GAMETEST.setDiscount(19.99);
        assertEquals(19.99, GAMETEST.getDiscount(), DELTA);
    }

    @Test
    public void setGenreTest() {
        GAMETEST.setGenre("MOBA");
        assertEquals("MOBA", GAMETEST.getGenre());
    }

    @Test
    public void setRatingTest() {
        GAMETEST.setRating(9.4);
        assertEquals(9.4, GAMETEST.getRating(), DELTA);
    }

    @Test
    public void setQuantityTest() {
        GAMETEST.setQuantity(160);
        assertEquals(160, GAMETEST.getQuantity());
    }

    @Test
    public void setIndexTest() {
      GAMETEST.setIndex(5);
      assertEquals(5, GAMETEST.getIndex());
    }

    @Test
    public void toStringTest() {
        String expected = "\nID: 1\nName: Forknight\nPrice: $15.99\nCreator: Ipec\nDiscount: 15.0\nGenre: FPS\nRating: 5.0\nQuantity: 200\n";
        assertEquals(expected, GAMETEST.toString());
    }
}
