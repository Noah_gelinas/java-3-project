package loaders;

import org.junit.Test;
import product.IProduct;
import product.Game;
import customers.Customer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import static org.junit.Assert.*;

public class FileLoaderTest {
  
  @Test
  public void loadProductsTest() throws IOException, FileNotFoundException {
    List<IProduct> product = new ArrayList<IProduct>();
    FileLoader loader = new FileLoader();
    product = loader.loadProducts("../Products.csv");
    Game productExpected = new Game(1, "Fortnite", 29.99, "Epic Games", 0.0, "Battle Royale", 4.8, 500);
    assertEquals(productExpected.toString(), product.get(0).toString());
  }

  @Test
  public void loadCustomersTest() throws IOException, FileNotFoundException {
    List<Customer> customers = new ArrayList<Customer>();
    FileLoader loader = new FileLoader();
    customers = loader.loadCustomers("../Customers.csv");
    Customer customerExpected = new Customer(1, "Noah", "Gelinas", 16100);
    assertEquals(customerExpected.toString(), customers.get(0).toString());
  }
}
