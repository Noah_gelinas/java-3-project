package customers;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {
  private final Customer CUSTOMER = new Customer(1, "John", "Baker", 300);

  @Test
  public void getIndexTest() {
    assertEquals(1, CUSTOMER.getIndex());
  }

  @Test
  public void getFirstNameTest() {
    assertEquals("John", CUSTOMER.getFirstName());
  }

  @Test
  public void getLastNameTest() {
    assertEquals("Baker", CUSTOMER.getLastName());
  }

  @Test
  public void getPointsTest() {
    CUSTOMER.setPoints(300);
    assertEquals(300, CUSTOMER.getPoints());
  }

  @Test
  public void setIndexTest() {
    CUSTOMER.setIndex(3);
    assertEquals(3, CUSTOMER.getIndex());
  }

  @Test
  public void setFirstNameTest() {
    CUSTOMER.setFirstName("Michael");
    assertEquals("Michael", CUSTOMER.getFirstName());
  }

  @Test
  public void setLastNameTest() {
    CUSTOMER.setLastName("Smith");
    assertEquals("Smith", CUSTOMER.getLastName());
  }

  @Test
  public void setPointsTest() {
    CUSTOMER.setPoints(5000);
    assertEquals(5000, CUSTOMER.getPoints());
  }

  @Test
  public void getCustomerDataTest() {
    assertEquals("1,John,Baker,300", CUSTOMER.getCustomerData());
  }

  @Test
  public void toStringTest() {
    String expectedString = "\nID: 1\nFirst Name: John\nLast Name: Baker\nPoints: 300";
    assertEquals(expectedString, CUSTOMER.toString());
  }
}
