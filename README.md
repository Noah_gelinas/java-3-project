# Java 3 project

## Authors: Noah Gelinas & Jaypee Tello

## Project Description

This program allows an employee or an admin to manage inventory. They are allowed to read, update, create, and delete products and customers depending on their roles. They can as well insert an order for a customer to gain points, which will also update the previous quantity of the product that was ordered. It will be focusing more on using csv files to attain and save the important information about the products and customers but it as well has a little bit of database.

## Project Structure

The Project is organized into several files and directories

It will all be inside the javaproject package, which is a Maven project.

In the main/java directory:

comparer: directory that has all the classes used to compare different objects in unique ways.
customers: directory that has the Customer class.
javaproject: directory that have both the application (UI) to be used according to role (Admin or Employee) as well as the GameStore (Business) class, which will be doing all the functions/actions on the products/customers objects.
loaders: directory that has all the needed classes to load or send data from/to a csv file.
product: directory that has the products classes (Game class and Merchandise class).


In the test directory:

All the JUnit tests for all the methods of all the classes organized by the directory of tested class.

## Features and Functionalities 

- Can read a product or customer in different ways. For example, you can read a game/games specifically through searching its name, creator, genre, etc. You can also read by ascending or descending order based on price, raitngs, etc. You also can read all the games if you want to.
- Can update a product or customer based on their ID. You can update their appropriate fields and the program is built not to crash when inserting invalid data.
- Can delete a customer or product based on their ID.
- Can create a new product or customer and save it to its csv file.
- Can also insert a new order for a customer, which will ask the id of the product to order, the id of the customer who is ordering, and the number of products they want to order. Once done, the program will register the points won on the customer who ordered's account based on the quantity of orders they made (quantity ordered * 20 = points). Then, the quantity ordered will be subtracted from the available quantity of the product ordered.

