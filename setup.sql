-- DROP TABLE Area

DROP TABLE Java_Customers;
DROP TABLE Java_Games;
DROP TABLE Java_Merchandise;

/
-- CREATE TABLE Area
CREATE TABLE Java_Customers (
    customer_id     NUMBER(6)       PRIMARY KEY,
    firstname       VARCHAR2(30),
    lastname        VARCHAR2(30),
    points          NUMBER(6)
);

CREATE TABLE Java_Games (
    game_id     NUMBER(6,0) PRIMARY KEY,
    name        VARCHAR2(30),
    price       NUMBER(7,2),
    creator     VARCHAR2(30),
    discount    NUMBER(5,2),
    genre       VARCHAR2(20),
    rating      NUMBER(2,1),
    quantity    NUMBER(6,0)
);

CREATE TABLE Java_Merchandise (
    merch_id    NUMBER(6,0) PRIMARY KEY,
    merch_size  VARCHAR2(1),
    name        VARCHAR2(30),
    price       NUMBER(9,2),
    creator     VARCHAR2(30),
    discount    NUMBER(5,2),
    quantity    NUMBER(7,0)
);

-- INSERT INTO TABLE Area
/
--INSERT INTO CUSTOMERS
INSERT INTO Java_Customers (customer_id,firstname,lastname,points)
VALUES(1,'Noah','Gelinas',0);

INSERT INTO Java_Customers (customer_id,firstname,lastname,points)
VALUES(2,'Jaypee','Tello',0);

INSERT INTO Java_Customers (customer_id,firstname,lastname,points)
VALUES(3,'Rida','Charaani',0);

--INSERT INTO GAMES
INSERT INTO Java_Games (game_id,name,price,creator,discount,genre,rating,quantity)
VALUES (1,'Fortnite',10.99,'Epic Games',0.0,'Battle Royale',4.8,999);

INSERT INTO Java_Games (game_id,name,price,creator,discount,genre,rating,quantity)
VALUES (2,'Rainbow 6 Siege',15.99,'Ubisoft',2.0,'FPS',3.7,870);

INSERT INTO Java_Games (game_id,name,price,creator,discount,genre,rating,quantity)
VALUES (3,'Valorant',20.0,'Riot Games',10.0,'FPS',4.0,769);

INSERT INTO Java_Games (game_id,name,price,creator,discount,genre,rating,quantity)
VALUES (4,'Minecraft',5.0,'Mojang',15.0,'Adventure',5.0,690);

INSERT INTO Java_Games (game_id,name,price,creator,discount,genre,rating,quantity)
VALUES (5,'Super Mario',9.99,'Nintendo',0.0,'Fantasy',5.0,550);

--INSERT INTO MERCHANDISE
INSERT INTO Java_Merchandise(merch_id,merch_size,name,price,creator,discount,quantity) 
VALUES (1,'S','Nike Hoodie',89.99,'Nike',20.0,167);

INSERT INTO Java_Merchandise(merch_id,merch_size,name,price,creator,discount,quantity) 
VALUES (2,'M','Yeezy Shirt',12.0,'Kanye East',10.0,232);

INSERT INTO Java_Merchandise(merch_id,merch_size,name,price,creator,discount,quantity) 
VALUES (3,'L','Adidas Tracksuit',100.0,'Adidas',0.0,367);

INSERT INTO Java_Merchandise(merch_id,merch_size,name,price,creator,discount,quantity) 
VALUES (4,'M','Puma Hat',20.0,'Puma',5.0,428);

INSERT INTO Java_Merchandise(merch_id,merch_size,name,price,creator,discount,quantity) 
VALUES (5,'M','Nike Socks 2.0',15.0,'Nike',10.0,537);